"""Arvujada liitmismäng."""


import random


class NumericGame:
    """Mängu sisu."""

    def __init__(self, score):
        """Vajalikud muutujad."""
        self.number_row = []
        self.number_highest_value = 7
        self.number_row_len = 5
        self.number_to_add = random.randint(1, self.number_highest_value)
        self.next_number = random.randint(1, self.number_highest_value)
        self.level = 0
        self.modulo_nr = 0
        self.number_of_addition = 1
        self.added_correctly = False
        self.score = score
        self.is_over = False

    def create_number_row(self):
        """
        Loob algse numbrirea, millega mängida.

        :return:
        """
        self.number_row.append(random.randint(1, self.number_highest_value))
        for i in range(1, self.number_row_len):
            next_number = 0
            while True:
                next_number = random.randint(1, self.number_highest_value)
                if next_number != self.number_row[i - 1]:
                    break
            self.number_row.append(next_number)

    def get_number_row(self):
        """
        Tagastab mänguvälja.

        :return:
        """
        return self.number_row

    def create_next_numbers(self):
        """
        Paneb järjekorras oodanud arvu järgmiseks ning leiab ülejärgmiseks uue arvu.

        :return:
        """
        self.number_to_add = self.next_number
        if (self.number_of_addition % 4) == 0:
            self.next_number = self.number_row[0] - self.number_row[1] + self.number_to_add
            if self.next_number == 0:
                self.next_number = 1
        else:
            self.next_number = random.randint(1, self.number_highest_value)
        self.number_of_addition += 1
        self.added_correctly = False

    def get_number_to_add(self):
        """
        Tagastab arvu.

        :return:
        """
        return self.number_to_add

    def get_next_number(self):
        """
        Tagastab järjekorras oleva arvu.

        :return:
        """
        return self.next_number

    def one_move(self, user_input):
        """
        Liidab uue arvu mängulaual oleva ja kasutaja poolt määratud arvuga.

        :param user_input:
        :return:
        """
        self.number_row[int(user_input)] += self.number_to_add
        if self.level > 0:
            self.number_row[int(user_input)] %= self.modulo_nr
        self.check_similar_numbers()
        self.set_number_row_len()
        if self.number_row_len == 1:
            self.is_over = True
        self.added_correctly = True

    def check_similar_numbers(self):
        """
        Kontrollib, kas kõrvutiolevad arvud mängulaual on samad. Kui on, siis koondab need. Vastavalt skoorib.

        :return:
        """
        same_number = []
        for j in range(len(self.number_row) - 1):
            if self.number_row[j] == self.number_row[j + 1]:
                same_number.append(j)
                self.score.add_to_score(self.level)
        if same_number == []:
            self.score.subtract_from_score(self.level)
        for element in same_number:
            del self.number_row[element]

    def set_number_row_len(self):
        """
        Täpsustab mänguvälja rea pikkuse peale iga käiku.

        :return:
        """
        self.number_row_len = len(self.number_row)

    def get_number_row_len(self):
        """
        Tagastab mänguvälja loendi pikkuse.

        :return:
        """
        return self.number_row_len

    def get_level(self):
        """
        Tagastab leveli, millel mäng parajasti on.

        :return:
        """
        return self.level

    def set_modulo_nr(self):
        """
        Määrab, mis on summa ülempiir.

        :return:
        """
        self.modulo_nr = self.level * 3 + 7

    def get_modulo_nr(self):
        """
        Tagastab summa ülempiiri.

        :return:
        """
        return self.modulo_nr

    def new_level(self):
        """
        Muudab mängu seadeid, kui on leveli vahetus.

        :return:
        """
        self.number_row = []
        self.level += 1
        if self.level == 1:
            self.number_row_len = 5
        elif self.level == 2:
            self.number_row_len = 7
            self.number_highest_value = 10
        elif self.level == 3:
            self.number_row_len = 9
            self.number_highest_value = 12
        self.number_of_addition = 1
        self.added_correctly = False
        self.is_over = False
        self.set_modulo_nr()
        self.number_to_add = random.randint(1, self.number_highest_value)
        self.next_number = random.randint(1, self.number_highest_value)

    def get_is_over(self):
        """
        Annab vastuse küsimusele, kas lõpetada mäng.

        :return:
        """
        return self.is_over


class Score:
    """Arvutab punktiskoori."""

    def __init__(self):
        """Vajalikud muutujad."""
        self.level_score = 0
        self.total_score = 0

    def start_score(self, level):
        """
        Määrab leveli alguses mingi punktiskoori kohe automaatselt.

        :return:
        """
        self.level_score = (1 + level) * 50

    def add_to_score(self, level):
        """
        Suurendab skoori, kui tehakse mängu edasi viiv liitmine.

        :param level:
        :return:
        """
        self.level_score += (1 + level) * 10

    def subtract_from_score(self, level):
        """
        Miinuspunktid, kui liitmise tulemusena mängväli väiksemaks ei jää.

        :param level:
        :return:
        """
        self.level_score -= (1 + level) * 2

    def time_into_score(self, time):
        """
        Võtab arvesse möödunud aega, ei tohi olla liiga aeglane.

        :param time:
        :return:
        """
        if time:
            self.level_score -= 1

    def get_level_score(self):
        """
        Tagastab leveli skoori.

        :return:
        """
        return self.level_score

    def calc_total_score(self):
        """
        Arvutab kogu mängu skoori.

        :return:
        """
        self.total_score += self.level_score
        self.level_score = 0

    def get_total_score(self):
        """
        Tagastab kogu mängu skoori.

        :return:
        """
        return self.total_score


class Results:
    """Kuvab kõik tulemused, ka varasemad."""

    def __init__(self):
        """Vajalikud muutujad."""
        self.results_from_file = []
        self.previous_results = []
        self.novel_result = []
        self.novel_pos = None

    def read_from_file(self, file):
        """
        Loeb andmed failist, kus on varasemad tulemused, previous_results sõnaraamatusse.

        :param file:
        :return:
        """
        inline = open(file, "r")
        for line in inline:
            self.results_from_file.append(line)

    def split_results(self):
        """
        Loob dictionary, kus on erinevate gruppide inimesed ära jagatud.

        :return:
        """
        for line in self.results_from_file:
            current_person = line.split(maxsplit=1)  # ajutine vahemuutuja, kuhu läheb praeguse persooni info
            self.previous_results.append(current_person)

    def add_new_result(self, name, result):
        """
        Lisab sõnaraamatusse just lõppenud mängu tulemuse.

        :param name:
        :param result:
        :return:
        """
        self.novel_result.append(str(result))
        self.novel_result.append(name)
        self.previous_results.append(self.novel_result)

    def write_file(self, file):
        """
        Kustutab olemasoleva info failis ja kirjutab uuesti tulemused faili.

        :return:
        """
        self.previous_results.sort(key=lambda x: int(x[0]), reverse=True)
        new_file = open(file, 'w')
        for i in self.previous_results:
            new_file.write(str(i[0]) + ' ' + i[1].replace('\n', '') + '\n')

    def find_pos_of_novel_result(self):
        """
        Leiab, millisele kohale lisati uus tulemus.

        :return:
        """
        for i in range(len(self.previous_results)):
            if self.novel_result == self.previous_results[i]:
                self.novel_pos = i
                break

    def get_novel_pos(self):
        """
        Tagastab positsiooni, milles paikneb kõige viimane tulemus.

        :return:
        """
        self.find_pos_of_novel_result()
        return self.novel_pos

    def get_results(self, name, result, file):
        """
        Käivitab õiges järjekorras kõik selle klassi meetodid ja kuvab tulemused.

        :param name:
        :param result:
        :param file:
        :return:
        """
        self.read_from_file(file)
        self.split_results()
        self.add_new_result(name, result)
        self.write_file(file)
        self.results_from_file = []
        self.read_from_file(file)
        return self.results_from_file
