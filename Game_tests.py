"""Mängu põhiloogika testimise fail."""


from Game import NumericGame
from Game import Score
from Game import Results
score2 = Score()
game2 = NumericGame(score2)
results2 = Results()


def test_create_number_row():
    """
    Testib esimest meetodit NumericGame klassis.

    :return:
    """
    game2.create_number_row()
    assert game2.number_row != []


def test_get_number_row():
    """
    Testib teist meetodit.

    :return:
    """
    assert len(game2.get_number_row()) == 5


def test_get_number_row_len():
    """
    Testib sellenimelist meetodit.

    :return:
    """
    assert game2.get_number_row_len() == 5


def test_create_next_numbers():
    """
    Testib sellenimelist meetodit - kas paneb olemasoleva ootejärjekorras oleva numbri esimeseks.

    :return:
    """
    previous_next_nr = game2.next_number
    game2.create_next_numbers()
    assert game2.number_to_add == previous_next_nr


def test_get_number_to_add():
    """
    Testib sellenimelist meetodit.

    :return:
    """
    assert game2.get_number_to_add() == game2.number_to_add


def test_get_next_number():
    """
    Testib sellenimelist meetodit.

    :return:
    """
    assert game2.get_next_number() == game2.next_number


def test_one_move():
    """
    Testib, kas arv liidetakse õiges positsioonis olevale mängulaua arvule.

    :return:
    """
    nr_row = game2.get_number_row()
    previous_nr = nr_row[1]
    game2.one_move(1)
    new_nr_row = game2.get_number_row()
    assert (previous_nr + game2.number_to_add) == new_nr_row[1]


def test_check_similar_numbers():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    current_score = score2.get_level_score()
    game2.one_move(0)
    new_score = score2.get_level_score()
    assert current_score != new_score


def test_set_number_row_len():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    game2.set_number_row_len()
    assert game2.number_row_len == len(game2.number_row)


def test_get_level():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    assert game2.get_level() == 0


def test_set_modulo_nr():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    game2.set_modulo_nr()
    assert game2.modulo_nr != 0


def test_get_modulo_nr():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    assert game2.get_modulo_nr() == 7


def test_new_level():
    """
    Testib vastavanimelist meetodit, mis peaks muuhulgas muutma ära modulo väärtuse.

    :return:
    """
    game2.new_level()
    assert game2.get_modulo_nr() == 10


def test_get_is_over():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    assert game2.is_over == game2.get_is_over()


def test_start_score():
    """
    Testib scoorimisklassi esimest meetodit.

    :return:
    """
    score2.start_score(2)
    assert score2.level_score == 150


def test_add_to_score():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    score2.add_to_score(2)
    assert score2.level_score == 180


def test_subtract_from_score():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    score2.subtract_from_score(2)
    assert score2.level_score == 174


def test_time_into_score():
    """
    Testib vastavanimelist meetodit, mis tiksutab skoori vähemaks, kui kulub 2s mänguaega.

    :return:
    """
    score2.time_into_score(True)
    assert score2.level_score == 173


def test_get_level_score():
    """
    Testib vastavanimelist meetodit.

    :return:
    """
    assert score2.get_level_score() == 173


def test_calc_total_score():
    """
    Testin meetodit, mis peaks käesoleva leveli punktiskoori liitma kogu punktiskoorile.

    :return:
    """
    score2.calc_total_score()
    assert score2.total_score == 173


def test_get_total_score():
    """
    Testin vastavanimelist meetodit.

    :return:
    """
    assert score2.get_total_score() == score2.total_score


def test_read_from_file():
    """
    Testin andmete failist lugemist.

    :return:
    """
    results2.read_from_file("results_for_testing.txt")
    assert "817 Kristi\n" in results2.results_from_file


def test_split_results():
    """
    Testin sellenimelist meetodit.

    :return:
    """
    results2.split_results()
    assert ["817", "Kristi\n"] in results2.previous_results


def test_add_new_result():
    """
    Testin, kas uue mänguskoori lisamine vanadele tulemustele töötab.

    :return:
    """
    results2.add_new_result("Piia", 795)
    assert ["795", "Piia"] in results2.previous_results


def test_write_file():
    """
    Testin faili kirjutamist.

    :return:
    """
    results2.write_file("results_for_testing.txt")
    file_lines = []
    inline = open("results_for_testing.txt", "r")
    for line in inline:
        file_lines.append(line)
    assert "795 Piia\n" in file_lines[1]


def test_find_pos_of_novel_result():
    """
    Testin, kas just lisatud tulemuse asukoht tulemuste tabelis määratakse õigesti.

    :return:
    """
    results2.find_pos_of_novel_result()
    assert results2.novel_pos


def test_get_novel_pos():
    """
    Testin, et lisatud tulemuse väärtus tagastatakse õigesti.

    :return:
    """
    assert results2.get_novel_pos() == 1


def test_get_results():
    """
    Testin viimast meetodit sellest klassist.

    :return:
    """
    results2.results_from_file = []
    results2.previous_results = []
    results2.novel_result = []
    final_results = results2.get_results("Aili", "966", "results_for_testing.txt")
    assert final_results[0] == "966 Aili\n"
