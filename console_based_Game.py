"""Arvujada liitmismäng."""


import random


class NumericGame:
    """Mängu sisu."""

    def __init__(self):
        """Vajalikud muutujad."""
        self.number_row = []
        self.number_highest_value = 7
        self.number_row_len = 5
        self.number_to_add = random.randint(1, self.number_highest_value)
        self.next_number = random.randint(1, self.number_highest_value)
        self.is_over = False
        self.added_correctly = False
        self.level = 0
        self.number_of_addition = 1
        self.level_score = 0
        self.total_score = 0

    def create_number_row(self):
        """
        Loob algse numbrirea, millega mängida.

        :return:
        """
        self.number_row.append(random.randint(1, self.number_highest_value))
        for i in range(1, self.number_row_len):
            next_number = 0
            while True:
                next_number = random.randint(1, self.number_highest_value)
                if next_number != self.number_row[i - 1]:
                    break
            self.number_row.append(next_number)

    def get_number_row(self):
        """
        Tagastab mänguvälja.

        :return:
        """
        return self.number_row

    def create_next_numbers(self):
        """
        Paneb järjekorras oodanud arvu järgmiseks ning leiab ülejärgmiseks uue arvu.

        :return:
        """
        self.number_to_add = self.next_number
        if (self.number_of_addition % 4) == 0:
            self.next_number = self.number_row[0] - self.number_row[1] + self.number_to_add
            if self.next_number == 0:
                self.next_number = 1
        else:
            self.next_number = random.randint(1, self.number_highest_value)
        self.number_of_addition += 1

    def get_number_to_add(self):
        """
        Tagastab arvu.

        :return:
        """
        return self.number_to_add

    def get_next_number(self):
        """
        Tagastab järjekorras oleva arvu.

        :return:
        """
        return self.next_number

    def one_move(self, input_string):
        """
        Liidab uue arvu mängulaual oleva ja kasutaja poolt määratud arvuga.

        :param input_string:
        :return:
        """
        if not input_string.isdigit():
            print("\n\n!!!Palun sisesta arv vahemikus 1 kuni {} (kaasa arvatud)!!!\n\n".format(self.number_row_len))
            self.added_correctly = False
        else:
            if 1 <= int(input_string) <= self.number_row_len:
                self.number_row[(int(input_string) - 1)] += self.number_to_add
                if self.level == 1:
                    self.number_row[(int(input_string) - 1)] %= 10
                elif self.level == 2:
                    self.number_row[(int(input_string) - 1)] %= 11
                elif self.level == 3:
                    self.number_row[(int(input_string) - 1)] %= 13
                elif self.level == 4:
                    self.number_row[(int(input_string) - 1)] %= 17
                elif self.level == 5:
                    self.number_row[(int(input_string) - 1)] %= 26
                elif self.level == 6:
                    self.number_row[(int(input_string) - 1)] %= 34
                same_number = []
                for i in range(len(self.number_row) - 1):
                    if self.number_row[i] == self.number_row[i + 1]:
                        same_number.append(i)
                for element in same_number:
                    del self.number_row[element]
                self.number_row_len = len(self.number_row)
                if self.number_row_len == 1:
                    self.is_over = True
                self.added_correctly = True
            else:
                print("\n\n!!!Palun sisesta arv vahemikus 1 kuni {} (kaasa arvatud)!!!\n\n".format(self.number_row_len))
                self.added_correctly = False

    def calculate_scores(self):
        """
        Arvutab punktiskoori leveli lõpus.

        :return:
        """
        self.level_score = (50 * (self.level + 1)) - self.number_of_addition
        self.total_score += self.level_score

    def return_level_score(self):
        """
        Tagastab punktiskoori.

        :return:
        """
        return self.level_score

    def return_total_score(self):
        """
        Tagastab punktiskoori.

        :return:
        """
        return self.total_score

    def game_over(self):
        """
        Annab vastuse küsimusele, kas lõpetada mäng.

        :return:
        """
        return self.is_over

    def new_level(self):
        self.number_row = []
        self.level += 1
        if self.level == 1:
            self.number_row_len = 5
        elif (self.level == 2):
            self.number_row_len = 7
            self.number_highest_value = 10
        elif (self.level == 3) or (self.level == 4):
            self.number_row_len = 8
            self.number_highest_value = 12
        else:
            self.number_row_len = 9
            self.number_highest_value = 20
        self.create_number_row()
        self.is_over = False
        self.number_of_addition = 1


class NumericGameInterface():
    """Mängijaga suhtleja."""

    def __init__(self, game):
        """Vajalikud muutujad."""
        self.current_game = game

    def start_message(self):
        """
        Mängu reeglid.

        :return:
        """
        print("Tere mängima Numeric mängu!")
        print("Liida uus arv olemasoleva arvujada ühe liikmega.")
        print("Kaks kõrvutiseisvat võrdset arvu taanduvad üheks arvuks.")
        print("Mängu võitmiseks peab mänguväljale alles jääma vaid üks arv.")

    def play_game(self):
        """
        Hakkame mängima.

        :return:
        """
        self.start_message()
        if self.current_game.level == 0:
            print("\n___Level 1___\nSoojenduseks teeme tavalist liitmist.\n")
        self.current_game.create_number_row()
        print("Mänguväli:  ")
        while True:
            for i in self.current_game.get_number_row():
                print(i, end="  ")
            print("\n\nLiida mõnele arvule arv {} (järgmine arv, mida peale seda liita, on {}).".format(self.current_game.get_number_to_add(), self.current_game.get_next_number()))
            player_answer = input("Mitmendale arvule uue arvu liidad?\n")
            self.current_game.one_move(player_answer)
            if self.current_game.game_over():
                self.current_game.calculate_scores()
                print("\n\nTubli!\nSaid selle leveli eest {} punkti. Kokku on sul {} punkti".format(self.current_game.return_level_score(), self.current_game.return_total_score()))
                self.current_game.new_level()
                if self.current_game.level == 7:
                    print("Mängu lõpp.")
                    break
                if input("Kas mängid veel? (kirjuta jah)") == "jah":
                    if self.current_game.level == 1:
                        print("\n\n\n___Level 2___\nNB! Iga summa taandatakse arvu 10 jäägiks.\n")
                    elif self.current_game.level == 2:
                        print("\n\n\n___Level 3___\nNB! Iga summa taandatakse arvu 11 jäägiks.\n")
                    elif self.current_game.level == 3:
                        print("\n\n\n___Level 4___\nNB! Iga summa taandatakse arvu 13 jäägiks.\n")
                    elif self.current_game.level == 4:
                        print("\n\n\n___Level 5___\nNB! Iga summa taandatakse arvu 17 jäägiks.\n")
                    elif self.current_game.level == 5:
                        print("\n\n\n___Level 6___\nNB! Iga summa taandatakse arvu 26 jäägiks.\n")
                    elif self.current_game.level == 6:
                        print("\n\n\n___Level 7___\nNB! Iga summa taandatakse arvu 34 jäägiks.\n")
                else:
                    break
            if self.current_game.added_correctly:
                self.current_game.create_next_numbers()


ngame = NumericGame()
interface = NumericGameInterface(ngame)
interface.play_game()
