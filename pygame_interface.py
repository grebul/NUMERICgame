"""Graafiline kasutajaliides."""


import pygame
import eztext
from Game import NumericGame
from Game import Score
from Game import Results


# --- mängus kasutusel olevad värvid ---
black = (0, 0, 0)
white = (255, 255, 255)
darkGray = (200, 200, 200)
lightGray = (240, 240, 240)
darkGreen = (70, 130, 0)
darkRed = (200, 0, 30)
lightGreen = (100, 175, 0)
lightRed = (255, 0, 60)
# --- üldparameetrid ---
display_width = 1200
display_height = 800
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('Numeric')
clock = pygame.time.Clock()


class TextOnScreen:
    """Kui vaja mingit teksti ekraanil kuvada."""

    def text_objects(self, text, font):
        """
        Leiab parameetrid teksti kuvamiseks ekraanile.

        :param text:
        :param font:
        :return:
        """
        text_surface = font.render(text, True, black)
        return text_surface, text_surface.get_rect()

    def message_display(self, text, width, height, size):
        """
        Määrab, kuidas teksti kuvatakse.

        :param text:
        :param width:
        :param height:
        :param size:
        :return:
        """
        text_param = pygame.font.Font('freesansbold.ttf', size)  # kuvatava teksti kirjatüüp ja suurus
        text_surface, text_rectangle = self.text_objects(text, text_param)  # sisend teisest meetodist
        text_rectangle.center = (width, height)  # kuidas määrata teksti paigutus
        gameDisplay.blit(text_surface, text_rectangle)  # blitting is doing a copy of one set of pixels onto another


class Button(TextOnScreen):
    """Teksti kuvamise alamklass, nupuvajutuse funktsionaalsusega."""

    def __init__(self):
        """Loob vajalikud muutujad."""
        self.is_clicked = 0

    def button(self, text, text_size, basic_color, active_color, x, y, width, height, function=None):
        """
        Loob interaktiivse nupu, millel on tekst ja mis käivitab vajutusel meetodi.

        :param text:
        :param text_size:
        :param basic_color:
        :param active_color:
        :param x:
        :param y:
        :param width:
        :param height:
        :param function:
        :return:
        """
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        if x + width > mouse[0] > x and y + height > mouse[1] > y:
            pygame.draw.rect(gameDisplay, active_color, (x, y, width, height))  # nö interaktiivne nupp
            if click[0] == 1:  # nupule vajutus
                self.is_clicked = 1
            if click[0] == 0 and self.is_clicked == 1 and function is not None:  # enam ei vajuta, aga just vajutatud
                self.is_clicked = 0
                function()  # jooksutame funktsiooni
        else:
            pygame.draw.rect(gameDisplay, basic_color, (x, y, width, height))
        TextOnScreen.message_display(self, text, (x + width / 2), (y + height / 2), text_size)


class GraphicalUserInterface:
    """Mida mängija näeb."""

    def __init__(self):
        """Loob vajalikud muutujad ja initsieerib mängu, punktiarvestuse ning tulemuste instantsid."""
        self.name = 'mängija'
        self.score = 0
        self.results1 = Results()
        self.score1 = Score()
        self.game1 = NumericGame(self.score1)
        self.user_name_from_input = eztext.Input(x=300, y=400, maxlength=45, color=darkRed, prompt='Sinu nimi: ')
        self.tracking_time = 0
        self.time_change = False

    def quitgame(self):
        """
        Lõpetab mängu.

        :return:
        """
        pygame.quit()
        quit()

    def game_intro1(self):
        """
        Avakuva.

        :return:
        """
        intro1 = True
        yes_button = Button()  # ekraanile kuvatavate tekstide ja nuppude loomine
        no_button = Button()
        title = TextOnScreen()
        subtitle = TextOnScreen()
        while intro1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quitgame()
            gameDisplay.fill(white)
            title.message_display('NUMERIC', (display_width / 2), (display_height / 2 - 100), 100)
            subtitle.message_display('Mängime?', (display_width / 2), (display_height / 2), 75)
            yes_button.button('JAH', 40, darkGreen, lightGreen, 200, 550, 200, 100, self.game_intro2)
            no_button.button('EI', 40, darkRed, lightRed, 800, 550, 200, 100, self.quitgame)
            pygame.display.update()
            clock.tick(60)

    def game_intro2(self):
        """
        Mängureeglite tutvustus.

        :return:
        """
        intro2 = True
        message1 = TextOnScreen()  # ekraanile kuvatavate tekstide instantside loomine
        message2 = TextOnScreen()
        message3 = TextOnScreen()
        message4 = TextOnScreen()
        ok_button = Button()
        while intro2:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quitgame()
            gameDisplay.fill(white)
            message1.message_display('Liida uus arv olemasoleva arvujada ühe liikmega.', (display_width / 2),
                                     (display_height / 2 - 150), 40)
            message2.message_display('Kaks kõrvutiseisvat võrdset arvu taanduvad üheks arvuks.', (display_width / 2),
                                     (display_height / 2 - 100), 40)
            message3.message_display('Iga leveli võitmiseks', (display_width / 2), (display_height / 2), 40)
            message4.message_display('peab mänguväljale alles jääma vaid üks arv.', (display_width / 2),
                                     (display_height / 2 + 50), 40)
            ok_button.button('OK', 40, darkGreen, lightGreen, 500, 550, 200, 100, self.level_display)
            pygame.display.update()
            clock.tick(60)

    def level_display(self):
        """
        Kuvab info algava leveli ning lõppenud leveli punktide kohta.

        :return:
        """
        level_info = True
        level_score = self.score1.get_level_score()
        self.score1.calc_total_score()
        total_score = self.score1.get_total_score()
        message1 = TextOnScreen()
        message2 = TextOnScreen()
        out_button = Button()
        start_button = Button()
        result_button = Button()
        while level_info:
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    self.quitgame()
            gameDisplay.fill(white)
            out_button.button('Välja', 40, darkRed, lightRed, 800, 550, 200, 100, self.quitgame)
            self.level_text(events, start_button, result_button)
            message1.message_display("Punkte eelmisest levelist: " + str(level_score), 1000, 30, 20)
            message2.message_display("Punkte kokku: " + str(total_score), 1000, 60, 20)
            pygame.display.update()
            clock.tick(60)

    def level_text(self, events, start_button, result_button):
        """
        Iga leveli spetsiifiline informatsioon.

        :return:
        """
        level_message = TextOnScreen()
        info_message = TextOnScreen()
        level = self.game1.get_level()
        if level < 4:
            level_message.message_display('LEVEL {}'.format(self.game1.level + 1), (display_width / 2),
                                          (display_height / 2 - 100), 75)
            start_button.button('Alusta', 40, darkGreen, lightGreen, 200, 550, 200, 100, self.game_loop)
        if level == 0:
            info_message.message_display('Sissejuhatuseks tavaline liitmine', (display_width / 2),
                                         (display_height / 2), 50)
        elif 4 > self.game1.level >= 1:
            info_message.message_display('Iga summa taandatakse arvu {} jäägiks'.format(self.game1.get_modulo_nr()),
                                         (display_width / 2), (display_height / 2), 50)
        elif level == 4:
            self.game_over_page(events, result_button)

    def game_over_page(self, events, result_button):
        """
        Kuvab, kui kõik levelid on läbi saanud.

        :return:
        """
        message = TextOnScreen()
        message.message_display('Mängu lõpp', (display_width / 2), (display_height / 2 - 100), 75)
        self.user_name_from_input.update(events)  # ezText sisend kasutajalt (nimi)
        self.user_name_from_input.draw(gameDisplay)
        self.name = self.user_name_from_input.get_input()  # salvestab selle nime, kui enter vajutada
        self.score = self.score1.get_total_score()  # annab ka punktiskoorile õige väärtuse
        result_button.button('Tulemused', 40, darkGreen, lightGreen, 200, 550, 250, 100, self.give_results_page)

    def give_results_page(self):
        """
        Punktiskooride paremusjärjestus.

        :return:
        """
        results_info = True
        result_list = self.results1.get_results(self.name, self.score, 'results.txt')  # listide list, kus on tulemused
        novel_pos = self.results1.get_novel_pos()  # muutuja, mis ütleb, mis positsioonil on kõige viimane tulemus
        message = TextOnScreen()
        out_button = Button()
        while results_info:
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    self.quitgame()
            gameDisplay.fill(white)
            out_button.button('Välja', 40, darkRed, lightRed, 800, 550, 200, 100, self.quitgame)
            for i in range(len(result_list)):  # kuvan tulemused mänguaknasse
                current_result = str(result_list[i].replace('\n', ''))
                if i == novel_pos:  # toon kõige viimase tulemuse välja natuke suuremas kirjas
                    message.message_display(current_result, (display_width / 2), ((1 + i) * 40), 30)
                else:
                    message.message_display(current_result, (display_width / 2), ((1 + i) * 40), 20)
            pygame.display.update()
            clock.tick(60)

    def game_loop(self):
        """
        Siin toimub põhiline mängimine.

        :return:
        """
        level_exit = self.game1.get_is_over()
        self.game1.create_number_row()
        start_ticks = pygame.time.get_ticks()
        self.score1.start_score(self.game1.get_level())
        number_button = Button()
        add_message = TextOnScreen()
        number_message = TextOnScreen()
        next_message = TextOnScreen()
        max_message = TextOnScreen()
        while not level_exit:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quitgame()
            gameDisplay.fill(white)
            self.game_board(number_button)
            self.prepare_for_next_cycle()
            self.game_number_info(add_message, number_message, next_message, max_message)
            self.scoring(start_ticks)
            pygame.display.update()
            clock.tick(60)

    def prepare_for_next_cycle(self):
        """
        Valmistab mängulaua ja numbrid ette järgmiseks liitmiseks.

        :return:
        """
        if self.game1.number_row_len == 1:  # väljub sellest levelist ja valmistab ette järgmise
            self.game1.new_level()
            self.level_display()
        if self.game1.added_correctly:
            self.game1.get_number_row()
            self.game1.create_next_numbers()

    def game_number_info(self, add_message, number_message, next_message, max_message):
        """
        Kuvab info liidetavate kohta.

        :param add_message:
        :param number_message:
        :param next_message:
        :param max_message:
        :return:
        """
        add_message.message_display("Liida", 200, display_height / 2 + 150, 40)
        number_message.message_display(str(self.game1.get_number_to_add()), 300, display_height / 2 + 150, 60)
        next_message.message_display("järgmine: {}".format(self.game1.get_next_number()), 230, display_height / 2 + 200,
                                     30)
        if self.game1.get_level() > 0:
            max_message.message_display("Max {}".format(self.game1.get_modulo_nr() - 1), 1000, display_height / 2 + 150,
                                        40)

    def game_board(self, number_button):
        """
        Mängulaud.

        :return:
        """
        for i in range(self.game1.number_row_len):  # kuvab mängulaua
            if i < len(self.game1.number_row):
                number_button.button(str(self.game1.number_row[i]), 40, darkGray, lightGray, (150 + i * 100),
                                     (display_height / 2 - 50), 70, 70, function=lambda: self.game1.one_move(i))

    def scoring(self, start_ticks):
        """
        Suhtleb skoorimisklassiga.

        :return:
        """
        seconds = int((pygame.time.get_ticks() - start_ticks) / 1000)  # arvutab, mitu s on möödas leveli algusest
        if seconds % 2 == 1:  # teeb vahemuutujad, et saaks skoori arvutamisel kulunud aega arvesse võtta
            self.tracking_time = 0
        elif seconds % 2 == 0:
            self.tracking_time += 1
        if self.tracking_time == 1:  # et ainult üks kord vähendataks skoori, iga kord, kui jälle 2s täis saab
            self.time_change = True
        elif (self.tracking_time == 0) or (self.tracking_time > 1):
            self.time_change = False
        self.score1.time_into_score(self.time_change)
        level_score = self.score1.get_level_score()
        message = TextOnScreen()
        message.message_display("Punkte sellest levelist: " + str(level_score), 1000, 30, 20)


def main():
    """
    Põhiprogramm, käivitab kõik muu.

    :return:
    """
    pygame.init()
    # --- initsieerin mängu klassi, mis paneb kõik muu ka käima ---
    gui = GraphicalUserInterface()
    gui.game_intro1()
    gui.quitgame()


main()
